// This file is part of the ACTS project.
//
// Copyright (C) 2016 ACTS project team
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

///////////////////////////////////////////////////////////////////
// Geant4DetElement.h, ACTS project, DD4hepDetector plugin
///////////////////////////////////////////////////////////////////

#ifndef ACTS_GEANT4PLUGINS_GEANT4DETECTORELEMENT_H
#define ACTS_GEANT4PLUGINS_GEANT4DETECTORELEMENT_H 1

namespace Acts {

/** @class Geant4DetElement

 */

class Geant4DetElement
{
public:
  /** Constructor */
  Geant4DetElement();
  /** Desctructor */
  virtual ~Geant4DetElement();

private:
};
}

#endif  // ACTS_GEANT4PLUGINS_GEANT4DETECTORELEMENT_H
