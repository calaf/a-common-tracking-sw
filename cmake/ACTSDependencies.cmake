# specify version numbers for dependencies of ACTS
set (ACTS_BOOST_VERSION "1.61")
set (ACTS_DOXYGEN_VERSION "1.8.11")
set (ACTS_ROOT_VERSION "6.06.04")