message (STATUS "The following variables can be set for this project.")
message (STATUS "They can be specified with '-D<VARIABLE>=<VALUE>' on the command line when invoking 'cmake'")
message (STATUS "Some variables are set to default values which are indicated by '[default value]'.")
message (STATUS "Examples/possible values for the different variables are given in parantheses.")
message (STATUS "")
message (STATUS "Required variables:")
message (STATUS "EIGEN_INCLUDE_DIR                   path to header files of Eigen library                    (e.g. /opt/eigen/3.2.8)")
message (STATUS "BOOST_ROOT                          path to root directory of boost installation             (e.g. /opt/boost/1.60.0)")
message (STATUS "")
message (STATUS "Optional variables:")
message (STATUS "ACTS_PARAMETER_DEFINITIONS_PLUGIN   path to header file with track parameter definitions     [ACTS/Utilities/detail/DefaultParameterDefinitions.h]")
message (STATUS "BUILD_DD4HEP_PLUGIN                 build DD4hep plugin                                      [OFF]  (ON|OFF)")
message (STATUS "BUILD_DOC                           build doxygen documentation                              [OFF]  (ON|OFF)")
message (STATUS "BUILD_TESTS                         build unit tests                                         [ON]   (ON|OFF)")
message (STATUS "BUILD_TGEO_PLUGIN                   build TGeo plugin                                        [OFF]  (ON|OFF)")
message (STATUS "BUILD_MATERIAL_PLUGIN               build Material plugin                                    [OFF]  (ON|OFF)")
message (STATUS "CMAKE_INSTALL_PREFIX                target installation directory                            (e.g. /opt/ACTS/)")
message (STATUS "PRINT_OPTIONS                       print this help")
message (STATUS "")
