#!/usr/bin/env bash

# abort on error
set -ex

# check for correct number of arguments
if [ $# -ne 3 ]
then
    echo "wrong number of arguments"
    echo "usage: test_coverage <ACTS_DIR> <BUILD_DIR> <OUTDIR>"
    exit 1
fi

# check for required environment variables
: ${gitlabToken:?"'gitlabToken' not set or empty"}
: ${gitlabTargetNamespace:?"'gitlabTargetNamespace' not set or empty"}
: ${gitlabTargetRepoName:?"'gitlabTargetRepoName' not set or empty"}
: ${gitlabMergeRequestId:?"'gitlabMergeRequestId' not set or empty"}
: ${BUILD_URL:?"'BUILD_URL' not set or empty"}

# set parameters
ACTS_DIR=$1
BUILD_DIR=$2
OUTDIR=$3

# check for ACTS
if [ ! -d "$ACTS_DIR" ]
then
    echo "ACTS_DIR='$ACTS_DIR' not found -> aborting"
    exit 1
fi

# clean up old files and run new tests
cd $BUILD_DIR
lcov -z -d .
make test CTEST_OUTPUT_ON_FAILURE=TRUE || echo "Done running unit tests"

# run lcov
lcov -c -t ACTS -o coverage.info --no-external -d `pwd` -d $ACTS_DIR
# remove test source files
lcov -r coverage.info "Tests/*" > tmp.info
mv tmp.info coverage.info
# generate HTML output
genhtml coverage.info -t ACTS --demangle-cpp -s --output-directory $OUTDIR > coverage.log

COMMENT="test coverage result:<br />"`tail -n 3 coverage.log | head -n 1`"<br />"
COMMENT="$COMMENT"`tail -n 2 coverage.log | head -n 1`"<br />"
COMMENT="$COMMENT"`tail -n 1 coverage.log`"<br />"
COMMENT="$COMMENT<br />full test results can be found [here]($BUILD_URL/artifact/MERGE/$OUTDIR/index.html)"

# publish result as comment
comment_merge_request add "$COMMENT" --project $gitlabTargetNamespace/$gitlabTargetRepoName --merge-request-id $gitlabMergeRequestId --token $gitlabToken
